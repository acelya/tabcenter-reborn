import { TinyColor, readability } from "@ctrl/tinycolor";

// fallbacks for theme colors
const CSS_TO_THEME_PROPS = {
  "--background": ["frame"],
  "--button-background-active": ["button_background_active"],
  "--button-background-hover": ["button_background_hover"],
  "--icons": ["icons", "toolbar_text", "bookmark_text", "tab_background_text", "tab_text"],
  "--tab-separator": [
    "tab_background_separator",
    "toolbar_field_separator",
    "toolbar_top_separator",
  ],
  "--tab-selected-line": ["tab_line", "tab_text", "tab_background_text"],
  "--tab-loading-indicator": ["tab_loading"],
  "--tab-active-background": ["tab_selected", "toolbar"],
  "--tab-active-text": ["tab_text", "toolbar_text", "bookmark_text", "tab_background_text"],
  "--tab-text": ["tab_background_text", "tab_text", "toolbar_text", "bookmark_text"],
  "--toolbar-background": ["toolbar", "frame"],
  "--toolbar-text": ["toolbar_text", "bookmark_text"],
  "--input-background": ["toolbar_field"],
  "--input-border": ["toolbar_field_border"],
  "--input-border-focus": ["toolbar_field_border_focus"],
  "--input-background-focus": ["toolbar_field_focus"],
  "--input-selected-text-background": ["toolbar_field_highlight", "button_background_active"],
  "--input-selected-text": ["toolbar_field_highlight_text", "toolbar_field_text"],
  "--input-text": ["bookmark_text", "toolbar_field_text"],
  "--input-text-focus": ["toolbar_field_text_focus", "toolbar_field_text"],
};

export function browserThemeToPanelTheme(theme) {
  const colors = theme.colors;
  const themeColors = {};

  if (colors) {
    for (const [cssVar, themeProps] of Object.entries(CSS_TO_THEME_PROPS)) {
      for (const prop of themeProps) {
        themeColors[cssVar] = null;
        if (colors[prop]) {
          themeColors[cssVar] = colors[prop];
          break;
        }
      }
    }
  }

  // swap background and tab-active-background colors if background is light
  if (
    themeColors["--background"] !== null &&
    new TinyColor(themeColors["--background"]).isLight()
  ) {
    if (themeColors["--tab-active-background"] !== null) {
      let tmp = themeColors["--background"];
      themeColors["--background"] = themeColors["--tab-active-background"];
      themeColors["--tab-active-background"] = tmp;

      tmp = themeColors["--tab-active-text"];
      themeColors["--tab-active-text"] = themeColors["--tab-text"];
      themeColors["--tab-text"] = tmp;
    }
  }

  // Since Firefox Color uses additional_backgrounds instead of theme_frame,
  // TCRn won’t fall back to default theme even if colors aren’t readable,
  // so the user won’t think that TCRn is buggy with regards to Firefox Color
  if (theme.images && theme.images.theme_frame && !isThemeReadable(themeColors)) {
    for (const cssVar of Object.keys(CSS_TO_THEME_PROPS)) {
      themeColors[cssVar] = null;
    }
  }

  return themeColors;
}

export function panelThemeToString(theme) {
  let stringPanelTheme = "";
  for (const [key, prop] of Object.entries(browserThemeToPanelTheme(theme))) {
    if (prop) {
      stringPanelTheme += `${key}: ${prop};\n`;
    }
  }

  return stringPanelTheme;
}

export function cssToThemeProps() {
  return JSON.parse(JSON.stringify(CSS_TO_THEME_PROPS));
}

function isThemeReadable(themeColors) {
  // if a value is not defined, we use its default value to check if it is actually readable
  return (
    isReadable(themeColors["--background"] || "#ffffff", themeColors["--tab-text"] || "#0c0c0d") &&
    isReadable(
      themeColors["--tab-active-background"] || "#d7d7db",
      themeColors["--tab-active-text"] || "#0c0c0d",
    ) &&
    isReadable(
      themeColors["--toolbar-background"] || "#f9f9fa",
      themeColors["--icons"] || "rgba(249, 249, 250, 0.8)",
    )
  );
}

// Some theme have bad contrast, but we only want to avoid incorrect themes
// So we don’t check constrast >= AA but some arbitrary value to avoid white on white…
function isReadable(color1, color2) {
  return readability(color1, color2) >= 2;
}
