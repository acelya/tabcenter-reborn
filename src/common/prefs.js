export async function getPrefs() {
  // migrate user settings from local storage to sync storage
  const localPrefs = await browser.storage.local.get();
  if (Object.keys(localPrefs).length !== 0) {
    // rename (typo) setting `compactModeMode` to `compactMode`
    localPrefs["compactMode"] = localPrefs["compactModeMode"];
    delete localPrefs["compactModeMode"];
    delete localPrefs["warnBeforeClosing"];

    browser.storage.sync.set(localPrefs);
    browser.storage.sync.remove("warnBeforeClosing");
    browser.storage.local.clear();
  }

  return browser.storage.sync.get({
    animations: true,
    themeIntegration: true,
    compactMode: 1 /* COMPACT_MODE_DYNAMIC */,
    compactPins: true,
    switchLastActiveTab: true,
    switchByScrolling: 0 /* SWITCH_BY_SCROLLING_WITH_CTRL */,
    notifyClosingManyTabs: true,
    useCustomCSS: true,
    customCSS: "",
  });
}
