/* global browser */

import Sidetab from "./sidetab.js";
import Tablist from "./tablist.js";
import Topmenu from "./topmenu.js";
import { extractNew } from "./utils.js";

import { getPrefs } from "../common/prefs.js";
import { cssToThemeProps, browserThemeToPanelTheme } from "../common/theme.js";

export default class Sidebar {
  constructor({ window, platform, prefs = {}, theme = {} }) {
    this.windowId = window.id;
    this.incognito = window.incognito;

    this.prefs = prefs;
    this._theme = theme;

    document.body.classList.toggle("incognito", window.incognito);
    document.body.setAttribute("platform", platform.os);

    this._topMenu = new Topmenu(this);
    this._tablist = new Tablist(this);

    browser.runtime.connect({ name: this.windowId.toString() });
    this._setupListeners();
  }

  static async factory() {
    const window = await browser.windows.getCurrent();
    const platform = await browser.runtime.getPlatformInfo();
    const prefs = await getPrefs();
    const theme = await browser.theme.getCurrent();
    return new Sidebar({ window, platform, prefs, theme });
  }

  search(val) {
    this._tablist.filter(val);
    this._topMenu.updateSearch(val);
  }

  createTab(props, options) {
    this._tablist.createTab(props, options);
  }

  _setupListeners() {
    browser.storage.onChanged.addListener(changes => {
      this._onStorageChanged(extractNew(changes));
    });

    if (browser.theme.onUpdated) {
      browser.theme.onUpdated.addListener(({ theme, windowId }) => {
        this._onThemeUpdated(theme, windowId);
      });
    }

    browser.runtime.onMessage.addListener((req, _sender, sendResponse) => {
      if (req.focusedWindowId === this.windowId) {
        if (req.req === "searchValue") {
          return sendResponse({ res: this._topMenu._searchBoxInput.value });
        }
        this.search(req.req);
      }
    });

    this._updateContextualIdentities();
    browser.contextualIdentities.onCreated.addListener(this._updateContextualIdentities);
    browser.contextualIdentities.onRemoved.addListener(this._updateContextualIdentities);
    browser.contextualIdentities.onUpdated.addListener(this._updateContextualIdentities);

    window.addEventListener("contextmenu", e => this._onContextMenu(e), false);
  }

  _onContextMenu(e) {
    const target = e.target;
    // Let the searchbox input and the tabs have a context menu.
    if (
      !(target && (target.id === "searchbox-input" || target.id.startsWith("newtab"))) &&
      !Sidetab.isTabEvent(e, false)
    ) {
      e.preventDefault();
    }
  }

  _onThemeUpdated(theme, windowId) {
    if (!windowId || windowId === this.windowId) {
      this._theme = theme;
      this._applyTheme(theme);
    }
  }

  _applyCustomCSS() {
    document.getElementById("customCSS").textContent = this.prefs.useCustomCSS
      ? this.prefs.customCSS
      : "";
  }

  _onStorageChanged(changes) {
    // merge prefs
    if (changes.hasOwnProperty("compactMode")) {
      changes.compactMode = parseInt(changes.compactMode);
    }
    if (changes.hasOwnProperty("switchByScrolling")) {
      changes.switchByScrolling = parseInt(changes.switchByScrolling);
    }
    Object.assign(this.prefs, changes);

    // apply changes
    if (changes.hasOwnProperty("customCSS") || changes.hasOwnProperty("useCustomCSS")) {
      this._applyCustomCSS();
    }
    if (changes.hasOwnProperty("themeIntegration")) {
      this._applyTheme(this._theme);
    }
    if (changes.hasOwnProperty("animations")) {
      document.body.classList.toggle("animated", this.prefs.animations);
    }
    if (changes.hasOwnProperty("compactPins")) {
      this._tablist.setCompactPins();
    }
    if (
      changes.hasOwnProperty("compactMode") ||
      changes.hasOwnProperty("compactPins") ||
      changes.hasOwnProperty("customCSS") ||
      changes.hasOwnProperty("useCustomCSS")
    ) {
      this._tablist._maybeShrinkTabs();
    }
  }

  _applyTheme(theme) {
    const style = document.body.style;

    // if theme integration is disabled or theme is not usable, remove css variables then return
    if (!this.prefs.themeIntegration) {
      for (const cssVar of Object.keys(cssToThemeProps())) {
        style.removeProperty(cssVar);
      }
      return;
    }

    // get the effective values we will be using
    const themeColors = browserThemeToPanelTheme(theme);

    // apply color if one was found, otherwise remove var and use default style
    for (const [cssVar, color] of Object.entries(themeColors)) {
      if (color !== null) {
        style.setProperty(cssVar, color);
      } else {
        style.removeProperty(cssVar);
      }
    }

    document.body.classList.toggle(
      "has-custom-input-color",
      themeColors["--input-selected-text"] !== null &&
        themeColors["--input-selected-text-background"] !== null,
    );
  }

  getContextualIdentityItems() {
    return this.identityItems;
  }

  _updateContextualIdentities() {
    browser.contextualIdentities.query({}).then(
      identities => {
        this.identityItems = identities.map(identity => {
          return {
            id: identity.cookieStoreId,
            title: identity.name,
            icons: { "16": `/sidebar/img/identities/${identity.icon}.svg#${identity.color}` },
          };
        });
      },
      () => {
        this.identityItems = [];
      },
    );
  }
}
