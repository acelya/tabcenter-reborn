// apply style ASAP!
if (location.search) {
  const search = decodeURIComponent(location.search);
  const theme = search.match(/theme=([^&]+)&?/);
  const custom = search.match(/custom=([^&]+)&?/);

  if (theme && theme[1]) {
    document.querySelector("body").setAttribute("style", theme[1]);
  }

  if (custom && custom[1]) {
    document.getElementById("customCSS").textContent = custom[1];
  }
}
