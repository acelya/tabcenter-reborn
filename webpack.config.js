/* global __dirname:true */
/* global module:true */

const path = require("path");

module.exports = {
  entry: {
    sidebar: "./src/sidebar/index.js",
    sidebarTheme: "./src/sidebar/theme.js",
    background: "./src/background/index.js",
  },
  output: {
    path: path.join(`${__dirname}/src`, "dist"),
    filename: "[name].bundle.js",
  },
  node: false,
};
